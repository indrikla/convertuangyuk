package com.Project.Project.ConvertYuk;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class SimulatorGUI extends Application {
    String[] negara = {"Indonesia", "USA", "Japan", "Malaysia", "Jerman"};
    String negaraAsal;
    String negaraTujuan;
    Integer nominalInt;
    Label resultLB = new Label("");


    @Override
    public void start(Stage stage) {
        AnchorPane ap = new AnchorPane();
        Scene main = new Scene(ap, 270, 350);
        stage.setTitle("ConvertUangYuk!");
        stage.setResizable(true);

        HBox hb = new HBox();
        hb.setLayoutX(40.0);
        hb.setLayoutY(98.0);
        hb.setPrefHeight(151.0);
        hb.setPrefWidth(189.0);

        VBox vb1 = new VBox();
        vb1.setPrefHeight(97.0);
        vb1.setPrefWidth(85.0);
        vb1.setSpacing(22.0);

        VBox vb2 = new VBox();
        vb2.setLayoutX(10.0);
        vb2.setLayoutY(10.0);
        vb1.setPrefHeight(151.0);
        vb2.setPrefWidth(110.0);
        vb2.setSpacing(10.0);
        VBox.setMargin(vb2, new Insets(10,40,0,0));

        Label fromLab = new Label("From");
        Label toLab = new Label("To");
        Label nominalLab = new Label("Nominal");

        ChoiceBox<String> negaraAsalCB = new ChoiceBox<>();
        negaraAsalCB.getItems().addAll(negara);
        negaraAsalCB.getSelectionModel().select(0);

        ChoiceBox<String> negaraTujuanCB = new ChoiceBox<>();
        negaraTujuanCB.getItems().addAll(negara);
        negaraTujuanCB.getSelectionModel().select(1);


        TextField nominal = new TextField();
        nominal.setPrefHeight(26.0);
        nominal.setPrefWidth(90.0);
        nominal.setText("0");
        Button submitButton = new Button();
        submitButton.setMnemonicParsing(false);
        submitButton.setText("Convert");


        //Action Result
        HBox resultHB = new HBox();
        resultHB.setLayoutX(35.0);
        resultHB.setLayoutY(275.0);
        resultHB.prefHeight(90.0);
        resultHB.prefWidth(200.0);
        resultHB.getChildren().add(resultLB);

        // Action kalo menekan Submit
        submitButton.setOnAction(event -> {
            if(!negaraAsalCB.getItems().isEmpty() && !negaraTujuanCB.getItems().isEmpty()) {
                negaraAsal = negaraAsalCB.getValue();
                negaraTujuan = negaraTujuanCB.getValue();
                nominalInt = Integer.valueOf(nominal.getText());
                InputOutput io = new InputOutput(negaraAsal, negaraTujuan, nominalInt);
                io.run();
                resultLB.setText(io.getResult());
            } else {
                resultLB.setText("");
            }
        });

        // Adds
        vb2.getChildren().addAll(negaraAsalCB, negaraTujuanCB, nominal,submitButton);
        vb1.getChildren().addAll(fromLab, toLab, nominalLab);
        hb.getChildren().addAll(vb1, vb2);
        ap.getChildren().addAll(hb, resultHB);

        stage.setScene(main);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

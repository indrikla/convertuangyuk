package com.Project.Project.ConvertYuk;

public class Japan extends Convert{

    private final double usD, rupiah, ringgit, euro;
    private String negaraTujuan;

    public Japan(String negaraTujuan, double yen) {
        super(yen);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.0092;
        this.ringgit = 0.039;
        this.rupiah = 129.9;
        this.euro = 0.0081;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        } else if (negaraTujuan.equalsIgnoreCase("Jerman")) {
            asal.setOutputan(asal.getInputan()*this.euro);
        } else {
            asal.setOutputan(asal.getInputan());
        }
    }

    public String toString() {
        return String.format("%.2f Yen : %.2f %s", getInputan(),this.getOutputan(),getMataUang(negaraTujuan));
    }

}
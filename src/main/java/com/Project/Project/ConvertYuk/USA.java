package com.Project.Project.ConvertYuk;

public class USA extends Convert{

    private final double rupiah, japanYen, ringgit, euro;
    private String negaraTujuan;

    public USA(String negaraTujuan, double usD) {
        super(usD);
        this.negaraTujuan = negaraTujuan;
        this.rupiah = 14175.5;
        this.japanYen = 108.86;
        this.ringgit = 4.28;
        this.euro = 0.89;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("japan")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        } else if (negaraTujuan.equalsIgnoreCase("Jerman")) {
            asal.setOutputan(asal.getInputan()*this.euro);
        } else {
            asal.setOutputan(asal.getInputan());
        }
    }

    public String toString() {

        return String.format("%.2f US Dollar : %.2f %s", getInputan(),this.getOutputan(),getMataUang(negaraTujuan));
    }

}
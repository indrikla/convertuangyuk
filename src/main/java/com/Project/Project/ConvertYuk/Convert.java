package com.Project.Project.ConvertYuk;

public abstract class Convert {

	private double inputan;
	private double outputan = 0.0;

	public Convert(double inputan) {
		this.inputan = inputan;
	}

	public abstract void converter(Convert asal);

	public abstract String toString();

	public void setInputan(double inputan) {
		this.inputan = inputan;
	}

	public double getInputan() {
		return inputan;
	}

	public void setOutputan(double outputan) {
		this.outputan = outputan;
	}

	public double getOutputan() {
		return outputan;
	}

	public String getMataUang(String negara){
		if(negara.equalsIgnoreCase("Indonesia")){
			return "Rupiah";
		} else if(negara.equalsIgnoreCase("USA")) {
			return "US Dollar";
		} else if(negara.equalsIgnoreCase("JAPAN")) {
			return "Yen";
		} else if (negara.equalsIgnoreCase("Malaysia")) {
			return "Ringgit";
		} else if (negara.equalsIgnoreCase("Jerman")) {
			return "Euro";
		} else {
			return "";
		}
	}
}

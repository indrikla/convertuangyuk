package com.Project.Project.ConvertYuk;

public class Malaysia extends Convert{

    private final double usD, japanYen, rupiah, euro;
    private String negaraTujuan;

    public Malaysia(String negaraTujuan, double ringgit) {
        super(ringgit);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.23;
        this.japanYen = 25.53;
        this.rupiah = 3306.34;
        this.euro = 0.21;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("japan")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("Jerman")) {
            asal.setOutputan(asal.getInputan()*this.euro);
        } else {
            asal.setOutputan(asal.getInputan());
        }
    }

    public String toString() {

        return String.format("%.2f Ringgit : %.2f %s", getInputan(),this.getOutputan(),getMataUang(negaraTujuan));
    }
}
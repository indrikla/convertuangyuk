package com.Project.Project.ConvertYuk;

public class InputOutput{

    protected String negaraAsal;
    protected String negaraTujuan;
    protected int nominal;
    private String result;

    public InputOutput(String negaraAsal, String negaraTujuan, int nominal){
        this.negaraAsal = negaraAsal;
        this.negaraTujuan = negaraTujuan;
        this.nominal = nominal;
    }

    public String getResult(){
        return result;
    }

    public void run() {
        Create mataUang = new Create();
        result = "";
        if (negaraAsal.equalsIgnoreCase("Indonesia")) {
            result = mataUang.createObject("Indonesia", negaraTujuan, nominal).toString();
        } else if (negaraAsal.equalsIgnoreCase("USA")) {
            result = mataUang.createObject("USA", negaraTujuan, nominal).toString();
        } else if (negaraAsal.equalsIgnoreCase("Japan")) {
            result = mataUang.createObject("Japan", negaraTujuan, nominal).toString();
        } else if (negaraAsal.equalsIgnoreCase("Malaysia")) {
            result = mataUang.createObject("Malaysia", negaraTujuan, nominal).toString();
        } else if (negaraAsal.equalsIgnoreCase("Jerman")) {
            result = mataUang.createObject("Jerman", negaraTujuan, nominal).toString();
        }
    }
}
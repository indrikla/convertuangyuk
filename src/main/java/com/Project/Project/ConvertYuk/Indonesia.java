package com.Project.Project.ConvertYuk;

public class Indonesia extends Convert{

    private final double usD, japanYen, ringgit, euro;
    private String negaraTujuan;

    public Indonesia(String negaraTujuan, double rupiah) {
        super(rupiah);
        this.negaraTujuan = negaraTujuan;
        this.usD = 0.000071;
        this.japanYen = 0.0077;
        this.ringgit = 0.0003;
        this.euro = 0.000063;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("japan")) {
            asal.setOutputan(asal.getInputan()*this.japanYen);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        } else if (negaraTujuan.equalsIgnoreCase("Jerman")) {
            asal.setOutputan(asal.getInputan()*this.euro);
        } else {
            asal.setOutputan(asal.getInputan());
        }
    }

    public String toString() {

        return String.format("%.2f Rupiah : %.2f %s", getInputan(),this.getOutputan(),getMataUang(negaraTujuan));
    }

}
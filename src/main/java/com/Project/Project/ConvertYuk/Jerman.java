package com.Project.Project.ConvertYuk;

public class Jerman extends Convert{

    private final double usD, rupiah, ringgit, yen;
    private String negaraTujuan;

    public Jerman(String negaraTujuan, double euro) {
        super(euro);
        this.negaraTujuan = negaraTujuan;
        this.usD = 1.13;
        this.ringgit = 4.83;
        this.rupiah = 15985.13;
        this.yen = 123.39;
    }

    public void converter(Convert asal) {
        if (negaraTujuan.equalsIgnoreCase("indonesia")) {
            asal.setOutputan(asal.getInputan()*this.rupiah);
        } else if (negaraTujuan.equalsIgnoreCase("usa")) {
            asal.setOutputan(asal.getInputan()*this.usD);
        } else if (negaraTujuan.equalsIgnoreCase("malaysia")) {
            asal.setOutputan(asal.getInputan()*this.ringgit);
        } else if (negaraTujuan.equalsIgnoreCase("japan")) {
            asal.setOutputan(asal.getInputan()*this.yen);
        } else {
            asal.setOutputan(asal.getInputan());
        }
    }

    public String toString() {
        return String.format("%.2f Euro : %.2f %s", getInputan(),this.getOutputan(),getMataUang(negaraTujuan));
    }

}